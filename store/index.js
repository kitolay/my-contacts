export const state = () => ({
  apiUrl : 'https://back-my-contacts.herokuapp.com/'
})

export const mutations = {
  SET_VAR_1 (state, value) {
    console.log('SET_VAR_1', value)
    state.apiUrl = value
  }
}