export default {
    components: true,
    css: [
        '~/assets/css/main.css',
        '~/assets/dist/bootstrap/bootstrap.min.css'        
    ],
    plugins: [
        '~/plugins/fontawesome.js',
    ],
    modules: [
        '@nuxtjs/toast'
       ],     
    toast: {           
           register: [ // Register custom toasts
             {
               name: 'my-error',
               message: 'Oops...Something went wrong',
               options: {
                 type: 'error'
               }
             }
           ]
    }
}